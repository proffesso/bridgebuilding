﻿using System.IO;
using System.Linq;
using BridgeSample;
using BridgeSample.DataProvider;
using BridgeSample.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MinimalPath.Test
{
    [TestClass]
    public class BuildingTest
    {
        [TestMethod]
        public void TestCorrectness()
        {
            var solver = new Solver();
            var days = solver.EstimateBuilding(new StringDataProvider(7, 3, "1 3 1 4 2 5"));

            Assert.AreEqual(days, 4);
        }

        [TestMethod]
        public void TestCorrectness2()
        {
            var solver = new Solver();
            var days = solver.EstimateBuilding(new StringDataProvider(10 , 1, "1 9 2 8 3 7 4 1 5 "));

            Assert.AreEqual(days, -1);
        }  

        [TestMethod]
        public void TestCorrectness3()
        {
            var solver = new Solver();
            var days = solver.EstimateBuilding(new StringDataProvider(10 , 1, "1 9 2 8 3 7 4 1 5 6 8 9 4"));

            Assert.AreEqual(days, 10);
        }  

        [TestMethod]
        public void TestZeroDays()
        {
            var solver = new Solver();
            var days = solver.EstimateBuilding(new StringDataProvider(3, 3, "1 3 1 4 2 5"));

            Assert.AreEqual(days, 0);
        }  
        
        [TestMethod]
        public void TestZeroSectionLength()
        {
            var solver = new Solver();
            var days = solver.EstimateBuilding(new StringDataProvider(3, 0, "1 3 1 4 2 5"));

            Assert.AreEqual(days, -1);
        }          

        [TestMethod]
        public void TestZeroLength()
        {
            var solver = new Solver();
            var days = solver.EstimateBuilding(new StringDataProvider(0, 3, "1 3 1 4 2 5"));

            Assert.AreEqual(days, 0);
        }  
        
        [TestMethod]
        [ExpectedException(typeof(NegativeNumberException))]
        public void TestNegativeNumber()
        {
            var data = new StringDataProvider(7, 3, "1 -3 1 4 2 5");
            var t = data.ReadNextValue().ToList();
        }

        [TestMethod]
        [ExpectedException(typeof(NotNumberException))]
        public void TestTextInLine()
        {
            var data = new StringDataProvider(7, 3, "1 3 char 2 2 5");
            var t = data.ReadNextValue().ToList();
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void TestFileNotFound()
        {
            var data = new FileDataProvider(1, 1, "noFile.txt");

            var t = data.ReadNextValue().ToList();
        }
    }
}
