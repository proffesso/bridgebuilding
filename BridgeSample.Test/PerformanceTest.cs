﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BridgeSample;
using BridgeSample.DataProvider;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace MinimalPath.Test
{
    [TestClass]
    public class PerformanceTest
    {
        [TestMethod]
        public void TestPerformance()
        {
            Solver solver = new Solver();

            Stopwatch watch = Stopwatch.StartNew();

            solver.EstimateBuilding(new RandomDataProvider(100000, 3));

            watch.Stop();

            Assert.IsTrue(watch.Elapsed < TimeSpan.FromMilliseconds(500));
        }

    }
}
