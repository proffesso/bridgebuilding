﻿using System.Collections.Generic;

namespace BridgeSample.DataProvider
{
    /// <summary>
    /// Interface IDataProvider for reading schedule from abstract input
    /// </summary>
    public abstract class ADataProvider
    {
        public int BridgeLength { get; protected set; }
        public int MaxSectionLength { get; protected set; }

        public ADataProvider(int bridgeLength, int maxSectionLength)
        {
            BridgeLength = bridgeLength;
            MaxSectionLength = maxSectionLength;
        }

        /// <summary>
        /// Reads the next scheduled place for work from abstract input
        /// </summary>
        /// <returns>IEnumerable&lt;int&gt;.</returns>
        public abstract IEnumerable<int> ReadNextValue();
    }
}
