﻿using System;
using System.Collections.Generic;
using BridgeSample.Exceptions;

namespace BridgeSample.DataProvider
{
    public abstract class ATextDataProvider : ADataProvider
    {
        /// <summary>
        /// schedule.
        /// </summary>

        protected abstract string line { get; }

        protected ATextDataProvider(int bridgeLength, int maxSectionLength)
            : base(bridgeLength, maxSectionLength)
        {
        }

        /// <summary>
        /// Reads the schedule from text input
        /// </summary>
        /// 
        /// <returns>IEnumerable&lt;int&gt;.</returns>
        /// <exception cref="NegativeNumberException"></exception>
        /// <exception cref="NotNumberException"></exception>

        public override IEnumerable<int> ReadNextValue()
        {
            string[] splitLine = line.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            int[] numbers = new int[splitLine.Length];
            for (int i = 0; i < splitLine.Length; i++)
            {
                yield return ParserHelper.TryToParcePositive(splitLine[i]);
            }
        }


    }
}

