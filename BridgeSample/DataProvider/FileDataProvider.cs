﻿using System.IO;

namespace BridgeSample.DataProvider
{
    /// <summary>
    /// Class FileDataProvider which allows to read from the File.
    /// </summary>
    public class FileDataProvider : ATextDataProvider
    {

        /// <summary>
        /// The file path
        /// </summary>
        private readonly string filePath;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileDataProvider"/> class.
        /// </summary>
        /// <param name="filePath">The file path.</param>

        public FileDataProvider(int bridgeLength, int maxSectioLength, string filePath)
            : base(bridgeLength, maxSectioLength)
        {
            this.filePath = filePath;
        }

        protected override string line
        {
            get
            {
                if (!File.Exists(filePath))
                {
                    throw new FileNotFoundException(string.Format("No file at '{0}'", filePath), filePath);
                }

                return File.ReadAllText(filePath);
            }
        }
    }
}

