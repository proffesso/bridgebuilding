﻿namespace BridgeSample.Exceptions
{
    /// <summary>
    /// Class NegativeNumberException
    /// </summary>
    public class NegativeNumberException : ReadingException
    {
        public NegativeNumberException(string wrongValue) : base(wrongValue)
        {
        }

        protected override string Prefix
        {
            get { return "Negative number"; }
        }
    }
}
