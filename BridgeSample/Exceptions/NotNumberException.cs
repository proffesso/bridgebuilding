﻿namespace BridgeSample.Exceptions
{
    /// <summary>
    /// Class NotNumberException
    /// </summary>
    public class NotNumberException : ReadingException
    {
        public NotNumberException(string wrongValue) : base(wrongValue)
        {
        }

        protected override string Prefix
        {
            get { return "Invalid character(s) in number"; }
        }
    }
}
