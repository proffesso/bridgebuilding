﻿using System;
using System.Diagnostics;
using BridgeSample.DataProvider;

namespace BridgeSample
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch watch = Stopwatch.StartNew();
            Solver solver = new Solver();
            try
            {
                var data = DataFactory.GetDataProvider(args);

                int days = solver.EstimateBuilding(data);

                if (days < 0)
                    Console.WriteLine("Bridge NEVER  will be finished");
                else 
                    Console.WriteLine("Bridge will be finished in {0} day{1}", days.ToString(), (days%10 == 1 ? "" : "s"));
            }
            catch (Exception e)
            {
                ExceptionNotifier(e.Message.ToString());
            }

            watch.Stop();
            Console.WriteLine("Calculeted in {0}", watch.Elapsed);

            Console.ReadKey();
        }

        private static void ExceptionNotifier(string Message)
        {
            Console.WriteLine("There was an error: {0}", Message);
        }
    }
}
