﻿using System.Collections.Generic;
using System.IO;
using BridgeSample.DataProvider;

namespace BridgeSample
{
    public class Solver
    {
        private static HashSet<int> reachableSet;

        public Solver()
        {
        
        }

        public int EstimateBuilding(ADataProvider data)
        {
            var sectionLength = data.MaxSectionLength;
            var width = data.BridgeLength;

            if (sectionLength >= width) return 0;

            reachableSet = new HashSet<int> { 0 };

            TryToPushValue(width, sectionLength, 0);

            var counter = 0;
            foreach (var number in data.ReadNextValue())
            {
                counter++;
                TryToPushValue(width, sectionLength, number);
                if (reachableSet.Count == width + 1)
                {
                    return counter;
                }
            }
            return -1;
        }

        private static void TryToPushValue(int X, int D, int item)
        {
            for (var j = 1; j <= D; j++)
            {
                var dist = item + j;
                if (dist <= X)
                    reachableSet.Add(dist);
            }
        }

    }
}

