﻿using BridgeSample.Exceptions;

public class ParserHelper
{

    /// <summary>
    /// Class TryToParcePositive helper with parsing positive numer or throw corresponding exception.
    /// </summary>

    public static int TryToParcePositive(string stringNumber)
    {
        int number;
        if (int.TryParse(stringNumber, out number))
        {
            if (number < 0)
            {
                throw new NegativeNumberException(stringNumber);
            }
        }
        else
        {
            throw new NotNumberException(stringNumber);
        }

        return number;
    }

}

